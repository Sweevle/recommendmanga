# README #

### How do I get set up? ###
- run composer install
- Get Cloud Vision API key and Project ID enter it 
- Get a Discord bot token and client_id from discord
- Run the bot

### Making the bot read manga's ###
The bot reads the images from the images folder.
The sub folder names are taken as the Manga name. The bot will then scan and read each image inside the folders.
To make it read go to the index.php page.

### Getting the Google Tokens ###
[Get a service account on Google and get a .json key](https://cloud.google.com/vision/docs/common/auth)

### Getting the Discord Tokens ###
[Create your own bot](https://discordapp.com/developers/applications/me)

invite the bot your own server with:
https://discordapp.com/api/oauth2/authorize?client_id=your_bots_client_id&scope=bot&permissions=0

### Run the bot ###
Start the bot with a CLI client with the command 

```
#!sh

php run.php
```