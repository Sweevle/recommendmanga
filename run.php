<?php

include __DIR__ . '/vendor/autoload.php';
use GuzzleHttp\Client;

$discord = new \Discord\Discord([
    'token' => 'bot-token'
]);

//when a message is send
$discord->on('ready', function ($discord) {
    echo "Bot is ready.", PHP_EOL;
    
    //check if the message is !recc
    $discord->on('message', function ($message) {
        $check = preg_match("/(^!recc\s)/", $message->content);
        if ($check == 1) {
            $manga_name = preg_replace("/(!recc\s)/", "", $message->content);
            $recommendation = getReccommendation($manga_name);
            $message->channel->sendMessage(htmlentities($recommendation));
        }
    });
    
     //check if the message is !list
     // if it is, get a list of all the read/known Manga's
    $discord->on('message', function ($message) {
        $check_list = preg_match("/^!list\b/", $message->content);
        if ($check_list == 1) {
            $allmangas = getAllMangas();
            $titles = implode(", ", $allmangas);
            $message->channel->sendMessage(htmlentities("I've read the following manga's:\n". $titles));
        }
    });
    
    
     //check if the message is !help
     // return the possible commands of the bot
    $discord->on('message', function ($message) {
        $check_list = preg_match("/!help\b/", $message->content);
        if ($check_list == 1) {

            $message->channel->sendMessage(htmlentities("`!recc [manga to compare with]` - recommends you a manga to read \n`!list` - shows a list with read mangas"));
        }
    });

});

$discord->run();

function getAllMangas()
{
    $all_query = "SELECT * FROM Manga";
    $connect = mysqli_connect("localhost", "root", "", "mangarecommendation");
    $results = mysqli_query($connect, $all_query);
    $list = array();
    $titles = array();

    while ($row = mysqli_fetch_assoc($results)) {
        if ($row["MangaedenID"] != null) {
            array_push($list, $row["MangaedenID"]);
        } else {
            array_push($titles, $row["Title"]);
        }
    }


    $guzzle = new Client();
    foreach ($list as $id) {
        $response = $guzzle->get('http://www.mangaeden.com/api/manga/' . $id);
        $json = json_decode($response->getBody());
        array_push($titles, $json->title);
    }
    sort($titles);

    return $titles;
}

function getReccommendation($title)
{
    //Get input manga information
    $all_query = "SELECT * FROM manga WHERE Title = '" . $title . "'AND UPPER(manga.Title) LIKE '%".$title."%' ";
    $connect = mysqli_connect("localhost", "root", "", "mangarecommendation");
    $result = mysqli_query($connect, $all_query);
    if (mysqli_num_rows($result) > 0) {
        $manga_ID = mysqli_fetch_assoc($result);

        //Get associated tags of input manga
        $tags_query = "SELECT  manga.ID, manga.Title, tag.Tag, tag.ID FROM mangatags INNER JOIN manga ON mangatags.MangaID=manga.ID
                   INNER JOIN tag ON mangatags.TagID=tag.ID WHERE manga.ID = '" . $manga_ID['ID'] . "'";
        $tags_result = mysqli_query($connect, $tags_query);
        $tags = array();

        while ($row = mysqli_fetch_assoc($tags_result)) {
            if ($row["Tag"] != null) {
                array_push($tags, $row["ID"]);
            }
        }

        $mangas = array();
        //Get Series with same tags
        foreach ($tags as $tag) {
            $manga_with_tag = "SELECT manga.Title FROM mangatags INNER JOIN manga ON mangatags.MangaID=manga.ID INNER JOIN
                           tag ON mangatags.TagID=tag.ID WHERE tag.ID = '" . $tag . "'";
            $tags_result = mysqli_query($connect, $manga_with_tag);
            while ($row = mysqli_fetch_assoc($tags_result)) {
                if ($row["Title"] != null) {
                    if( $row["Title"] !== $title) {
                        array_push($mangas, $row["Title"]);
                    }
                }
            }
        }

        //If series has 2 or more tags in common return
        $frequency = array_count_values($mangas);
        arsort($frequency);

        $keys = array_keys($frequency);

        $morethantwo = array();
        foreach ($keys as $key){
            if ($frequency[$key] >= 2){
                array_push($morethantwo, $key);
            }
        }

        //pick a random out of the list
        $recommendation = $morethantwo[array_rand($morethantwo)];
        return $recommendation;

    } else {
        return "Unknown manga, check spelling or check the read list with the !list command";
    }
}