-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2017 at 11:39 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mangarecommendation`
--

-- --------------------------------------------------------

--
-- Table structure for table `manga`
--

CREATE TABLE `manga` (
  `ID` int(255) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `MangaedenID` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manga`
--

INSERT INTO `manga` (`ID`, `Title`, `MangaedenID`) VALUES
(118, '07-Ghost', '4e70ea1bc092255ef7004ce4'),
(119, '1/2 Prince', '4e70ea04c092255ef700475a'),
(120, '666 Satan', '4e70ea01c092255ef7004638'),
(121, 'Akagami No Shirayukihime', '4e70ea10c092255ef7004a96'),
(122, 'Alice 19th', '4e70e9c5c092255ef700381a'),
(123, 'Air Gear', '4e70ea0ac092255ef70048d8'),
(124, 'Akatsuki No Yona', '51a686cc45b9ef1aee6a6116'),
(125, 'Bungou Stray Dogs', '54bf0c4f45b9ef5f0e5c8731'),
(126, 'Eureka Seven', '4e70e995c092255ef7002e3b'),
(127, 'Eyeshield 21', '4e70ea03c092255ef7004702'),
(128, 'bleach', '4e70e9efc092255ef7004274'),
(129, 'Magi - Labyrinth of Magic', '5096b788c092254a33010478'),
(130, 'Perfect Girl Evolution', '4e70e9e9c092255ef7004078'),
(131, 'D.Gray Man', '4e70ea05c092255ef700477b'),
(132, 'All Out', ''),
(136, 'Shokugeki No Soma', '5166899845b9ef06ebc15ebc');

-- --------------------------------------------------------

--
-- Table structure for table `mangatags`
--

CREATE TABLE `mangatags` (
  `ID` int(255) NOT NULL,
  `MangaID` int(255) NOT NULL,
  `TagID` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mangatags`
--

INSERT INTO `mangatags` (`ID`, `MangaID`, `TagID`) VALUES
(156, 118, 1),
(157, 118, 2),
(158, 118, 3),
(159, 118, 4),
(160, 118, 6),
(161, 118, 8),
(162, 119, 1),
(163, 119, 2),
(164, 119, 5),
(165, 119, 3),
(166, 119, 4),
(167, 119, 6),
(168, 119, 7),
(169, 119, 8),
(170, 120, 3),
(171, 120, 6),
(173, 121, 1),
(174, 121, 7),
(175, 122, 3),
(176, 122, 6),
(177, 122, 7),
(178, 123, 2),
(179, 123, 3),
(180, 123, 6),
(181, 124, 1),
(182, 124, 2),
(183, 124, 3),
(184, 124, 6),
(185, 125, 3),
(186, 125, 6),
(187, 127, 5),
(188, 127, 3),
(189, 127, 4),
(190, 127, 6),
(191, 128, 2),
(192, 128, 3),
(193, 128, 6),
(194, 128, 8),
(195, 129, 3),
(196, 129, 6),
(199, 130, 3),
(200, 130, 6),
(201, 130, 7),
(202, 130, 8),
(203, 131, 2),
(204, 131, 3),
(205, 131, 6),
(206, 132, 5),
(207, 132, 4),
(208, 132, 6),
(212, 136, 5),
(214, 136, 4),
(216, 136, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tag`
--

CREATE TABLE `tag` (
  `ID` int(255) NOT NULL,
  `Tag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tag`
--

INSERT INTO `tag` (`ID`, `Tag`) VALUES
(1, 'Royal'),
(2, 'Religion'),
(3, 'Fighting'),
(4, 'Special Abilities'),
(5, 'Sports'),
(6, 'Alternate Universe'),
(7, 'Love'),
(8, 'School');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `manga`
--
ALTER TABLE `manga`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `mangatags`
--
ALTER TABLE `mangatags`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `MangaID` (`MangaID`),
  ADD KEY `TagID` (`TagID`);

--
-- Indexes for table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `manga`
--
ALTER TABLE `manga`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `mangatags`
--
ALTER TABLE `mangatags`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;
--
-- AUTO_INCREMENT for table `tag`
--
ALTER TABLE `tag`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `mangatags`
--
ALTER TABLE `mangatags`
  ADD CONSTRAINT `mangatags_ibfk_1` FOREIGN KEY (`MangaID`) REFERENCES `manga` (`ID`),
  ADD CONSTRAINT `mangatags_ibfk_2` FOREIGN KEY (`TagID`) REFERENCES `tag` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
