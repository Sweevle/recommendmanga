<?php



require_once "config.php";

$query = "SELECT mangatags.ID, manga.ID, manga.Title, tag.Tag, tag.ID FROM mangatags INNER JOIN manga ON mangatags.MangaID=manga.ID INNER JOIN tag ON mangatags.TagID=tag.ID";

$results = mysqli_query($connect, $query);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="css/foundation.css"/>
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/foundation.js"></script>
    <script src="js/foundation.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
</head>
<body>
    <table class="hover responsive list expanded">

    <thead>
    <tr>
        <th >Title</th>
        <th >Tag</th>
    </tr>
    </thead>
    <tbody>
    <!---- Display the tags per manga ----> 
    <?php foreach ($results as $item) {?>
    <tr>
        <td ><?= $item['Title'] ?></td>
        <td ><?= $item['Tag'] ?></td>
    </tr>
<?php } ?>
</tbody>
</table>

    <script>
        $(document).foundation();
    </script>
</body>
</html>
