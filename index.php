<?php

# Includes the autoloader for libraries installed with composer
require __DIR__ . '/vendor/autoload.php';
require_once "config.php";

# Imports the Google Cloud client library
use Google\Cloud\Vision\VisionClient;
use GuzzleHttp\Client;

# Your Google Cloud Platform project ID
$projectId = 'project_id';
// specify the path to your application credentials
putenv('GOOGLE_APPLICATION_CREDENTIALS=path_to_key');

$vision = new VisionClient([
    'projectId' => $projectId,
]);
$guzzle = new Client();

//tag keyword sets
$tags['Royal'] = ["prince", "princess", "king", "queen", "majesty", "imperial", "highness", "lord", "emperor", "empress", "palace",
    "ruler", "nation"];
$tags['Religion'] = ["church", "hell", "heaven", "bishop", "god", "soul", "exorcist", "apostle", "akuma", "demon", "reaper",
    "temple"];
$tags['Sports'] = ["tennis", "rugby", "game", "ball", "football", "points", "goal", "match", "contest"];
$tags['Fighting'] = ["battle", "fight", "protect", "military", "peace", "war", "strength", "battle", "fighter",
    "shield", "weapon", "duel", "punch", "sword", "kick", "general", "troops", "kill"];
$tags['Special abilities'] = ["rank", "ability", "hero", "technique", "training", "magic", "skill", "learn"];
$tags['Alternate Universe'] = ["level", "squad", "team", "hero", "ability", "game", "dragon", "demon", "spirit", "phoenix",
    "world", "members", "skill", "exp", "guild", "magic"];
$tags['Love'] = ["date", "radiant", "love", "crush", "boyfriend", "girlfriend", "relationship", "engaged", "wedding"];
$tags['School'] = ["school", "class", "lesson", "academy", "teacher", "student", "schooluniform", "homework","class"];

$mangas = [];

// GET NAMES OF MANGA
foreach (glob("images/*") as $filename) {
    array_push($mangas, str_replace("images/", "", $filename));
}

//get manga data and enter it in database
$guzzle = new Client();

//get json data from api
$response = $guzzle->get('http://www.mangaeden.com/api/list/0/');
$json = json_decode($response->getBody());

$mangaids = [];
foreach($mangas as $manga){
    $details = [];
    $details["title"] = $manga;
    $details["id"] = null;

    for ($i = 0; $i < count($json->manga); $i++ ){
        if ($json->manga[$i]->a == $details["title"]){
            $details["id"] = $json->manga[$i]->i;
        } elseif ($json->manga[$i]->t == $details["title"]){
            $details["id"] = $json->manga[$i]->i;
        }
    }

    array_push($mangaids, $details);
}

//enter manga into database
foreach ($mangaids as $detail=>$item) {
    $manga_query = "INSERT INTO `manga`(`Title`, `MangaedenID`) VALUES ('".$item["title"]."', '". $item["id"]."')". PHP_EOL;
    mysqli_query($connect, $manga_query);
    echo $manga_query . "<br>";
}

//put read tekst of each page into array
foreach ($mangas as $manga) {
    $imagestring = "";

    $files = glob('images/' . $manga . '/*', GLOB_NOSORT);
    ini_set('max_execution_time', 0);
    foreach ($files as $file) {
        $image = $vision->image(file_get_contents($file), ['TEXT_DETECTION']);
        $result = $vision->annotate($image);
        foreach ((array)$result->text() as $text) {
            $imagestring .= strtolower($text->description()) . " ";
        }
    }

    //exclude grammar words and odd combinations
    $prepositions = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
        "s", "t", "u", "v", "w", "x", "y", "z", "of", "at", "in", "to", "for", "on", "by", "over", "but", "up", "out",
        "doesnt", "since", "is", "and", "the", "with", "my", "it", "me", "was", "when", "this", "you", "be", "has",
        "mangareadernet", "getting", "manga", "thyaeriatranslations", "if", "from", "are", "that", "have", "not", "he",
        "as", "do", "so", "st", "his", "they", "we", "say", "her", "she", "or", "an", "will", "all", "there", "their",
        "what", "about", "who", "get", "go", "can", "no", "only", "net", "mangareader", "reader", "http", "would",
        "dont", "after", "should", "could", "com", "were", "ll", "thing", "called", "above", "beep", "lv", "chop",
        "ha", "heh", "just", "much", "around", "these", "here", "those", "oh", "also", "besides", "heard", "into",
        "become", "together", "us", "some", "re", "like", "thanks", "see", "sfx", "your", "him", "even", "now",
        "back", "why", "want", "how", "take", "well", "please", "did", "really", "let", "them", "must", "don", "more",
        "ve", "yeah", "haa", "hey", "got", "where", "sorry", "still", "any", "because", "come",
        "said", "thats", "okay", "its", "right", "one", "time", "know", "then", "been", "way", "isn", "th", "remember",
        "able", "wha", "than", "under", "wouldn", "ther", "ba", "kun", "our", "always", "think", "ever", "too", "didn",
        "may", "someone", "before", "again", "other", "going", "make", "use", "damn", "opt", "san", "sama", "dono",
        "ngh", "ya", "yadamn", "ii", "never", "stop", "ugh", "huh", "ing", "give", "ah",
        "man", "ikk", "eh", "came", "anything", "through", "ug", "bed", "case", "arri",
        "face", "wait", "gonna", "look", "something", "doing", "today", "already", "had", "tell",
        "yes", "away", "own", "things", "run", "running", "meet", "ness", "charge", "during",
        "best", "behind", "burnt", "ok", "until", "inside", "su", "need", "high", "good", "hmm", "left", "sense",
        "dry", "transmitted", "vribbl", "thank", "long", "prin", "cess", "ththump", "chan", "onee", "ur", "am", "tv",
        "charac", "thump", "try", "down", "isbn", "lfo", "co", "wanna", "english", "actually",
        "missropeway", "secondswall", "first", "neechan", "new", "block", "guys", "such", "bun", "feel", "due",
        "whoosh", "spam", "mention", "very", "second", "two", "help", "work", "sure", "home", "three", "hal", "far",
        "ugo", "scary", "gone", "finally", "ahh", "talk", "wearing", "wa", "ac", "er", "although", "definitely",
        "became", "ago", "off", "comawarea", "horned", "read", "wave", "scanlations", "cated", "toat", "snap", "et", "ne",
        "increase", "lise", "almost", "created", "illust", "increases", "sfr");

    foreach ($prepositions as &$preposition) {
        $preposition = '/\b' . preg_quote($preposition, '/') . '\b/';
    }
    $prepositions_removed = preg_replace($prepositions, '', $imagestring);
    $remaining_words = preg_replace("/lv\d*|[^a-zA-Z\d\s:]/", "", $prepositions_removed);

    echo "<h1>" . $manga . "</h1>";

    $frequency = array_count_values(str_word_count($remaining_words, 1));
    arsort($frequency);

    echo '<pre>';
    echo '<h2>10 most frequent words</h2>';
    print_r(array_slice($frequency, 0, 10));
    echo '</pre>';


//compare keywords of tags and frequent words
    $collect_tags = [];

    foreach ($tags as $tag => $item) {
        echo "<h2>".$tag."</h2>";
        $match_count = 0;
        foreach ($item as $keyword) {
            $re = '/\b' . $keyword . '/i';
            preg_match_all($re, $remaining_words, $matches);

            if (count($matches[0]) > 5) {
                $match_count++;
                if ($match_count >= 3) {
                    array_push($collect_tags, $tag);
                }
            }

            echo "<strong>".$keyword."</strong> matched ".count($matches[0])." times. <br>";
        }
    }
    $active_tags = array_unique($collect_tags);

    echo '<pre>';
    print_r($active_tags);
    echo '</pre>';

//get mangaid from database
    $mangaid_query = "SELECT `ID` FROM `manga` WHERE `Title` = '" . $manga . "'";
    $manga_result = mysqli_query($connect, $mangaid_query);
    $manga_ID = mysqli_fetch_assoc($manga_result);

    foreach ($active_tags as $active) {
        $tagid_query = "SELECT `ID` FROM `tag` WHERE `Tag` = '" . $active . "'";
        $tag_result = mysqli_query($connect, $tagid_query);
        $tag_ID = mysqli_fetch_assoc($tag_result);

        $mangatag_query = "INSERT INTO `mangatags`(`MangaID`, `TagID`) VALUES ('" . $manga_ID["ID"] . "', '" . $tag_ID["ID"] . "')" . PHP_EOL;
        echo $mangatag_query . "<br>";
        mysqli_query($connect, $mangatag_query);
    }
}